﻿# Longbow.Tasks

<a href="README.md">English</a> | <span>中文</span>

---

##### Gitee
[![Appveyor build](https://img.shields.io/endpoint.svg?logo=appveyor&label=build&color=blueviolet&url=https://ba.sdgxgz.com/api/Gitee/Builds?projName=longbow-tasks)](https://ci.appveyor.com/project/ArgoZhang/longbow-tasks)
[![Build Status](https://img.shields.io/appveyor/ci/ArgoZhang/longbow-tasks/master.svg?logo=appveyor&label=maser)](https://ci.appveyor.com/project/ArgoZhang/longbow-tasks)
[![Test](https://img.shields.io/appveyor/tests/ArgoZhang/longbow-tasks/master.svg?logo=appveyor&)](https://ci.appveyor.com/project/ArgoZhang/longbow-tasks/build/tests)
[![Issue Status](https://img.shields.io/endpoint.svg?logo=Groupon&logoColor=critical&label=issues&url=https://ba.sdgxgz.com/api/Gitee/Issues?userName=LongbowGroup%26repoName=Longbow.Tasks)](https://gitee.com/LongbowGroup/Longbow.Tasks/issues)
[![Pull Status](https://img.shields.io/endpoint.svg?logo=Groupon&logoColor=green&color=success&label=pulls&url=https://ba.sdgxgz.com/api/Gitee/Pulls?userName=LongbowGroup%26repoName=Longbow.Tasks)](https://gitee.com/LongbowGroup/Longbow.Tasks/pulls)
