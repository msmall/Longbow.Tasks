﻿using Cronos;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Longbow.Tasks
{
    public class CronTest
    {
        private readonly ITestOutputHelper _helper;

        public CronTest(ITestOutputHelper helper)
        {
            _helper = helper;
        }

        [Fact]
        public void Cron_Ok()
        {
            Assert.Equal("* * * * * *", Cron.Secondly());
            Assert.Equal("*/5 * * * * *", Cron.Secondly(5));
            Assert.Equal("0 * * * * *", Cron.Minutely());
            Assert.Equal("0 * * * *", Cron.Hourly());
            Assert.Equal("0 0 * * *", Cron.Daily());
            Assert.Equal("0 0 * * 1", Cron.Weekly());
            Assert.Equal("0 0 1 * *", Cron.Monthly());
            Assert.Equal("0 0 1 1 *", Cron.Yearly());
            Assert.Equal("0 0 31 2 *", Cron.Never());
        }

        [Fact]
        public void ParseCronExpression_Exception()
        {
            Assert.ThrowsAny<CronFormatException>(() => Cron.ParseCronExpression("* *"));
            Assert.ThrowsAny<ArgumentNullException>(() => Cron.ParseCronExpression(null));
        }

        [Fact]
        public void ParseCron_Ok()
        {
            var crop = Cron.Secondly().ParseCronExpression();
            Assert.Equal(Cron.Secondly(), crop.ToString());
        }

        [Fact]
        public void GetNextExecution_Ok()
        {
            Assert.Null(Cron.Never().ParseCronExpression().GetNextExecution());
        }

        [Fact]
        public void Secondly_Ok()
        {
            var cron = Cron.Secondly().ParseCronExpression();
            var now = DateTimeOffset.Now;
            var nextRuntimes = cron.GetOccurrences(now, now.AddMinutes(1), TimeZoneInfo.Local).Take(3).ToList();
            nextRuntimes.ForEach(d =>
            {
                _helper.WriteLine($"{d.ToString()}");
            });
            Assert.Equal(nextRuntimes[0].AddSeconds(1), nextRuntimes[1]);
            Assert.Equal(nextRuntimes[1].AddSeconds(1), nextRuntimes[2]);

            // 每 2 秒
            cron = "*/2 * * * * *".ParseCronExpression();
            nextRuntimes = cron.GetOccurrences(now, now.AddMinutes(1), TimeZoneInfo.Local).Take(3).ToList();
            nextRuntimes.ForEach(d =>
            {
                _helper.WriteLine(d.ToString());
            });
            Assert.Equal(nextRuntimes[0].AddSeconds(2), nextRuntimes[1]);
            Assert.Equal(nextRuntimes[1].AddSeconds(2), nextRuntimes[2]);

            // at 10 秒
            cron = "10 * * * * *".ParseCronExpression();
            nextRuntimes = cron.GetOccurrences(now, now.AddMinutes(5), TimeZoneInfo.Local).Take(3).ToList();
            nextRuntimes.ForEach(d =>
            {
                _helper.WriteLine(d.ToString());
            });
            Assert.Equal(nextRuntimes[0].AddSeconds(60), nextRuntimes[1]);
            Assert.Equal(nextRuntimes[1].AddSeconds(60), nextRuntimes[2]);

            // range 10-15
            cron = "10-13 * * * * *".ParseCronExpression();
            nextRuntimes = cron.GetOccurrences(now, now.AddMinutes(1), TimeZoneInfo.Local).Take(3).ToList();
            nextRuntimes.ForEach(d =>
            {
                _helper.WriteLine(d.ToString());
            });
            Assert.Equal(nextRuntimes[0].AddSeconds(1), nextRuntimes[1]);
            Assert.Equal(nextRuntimes[1].AddSeconds(1), nextRuntimes[2]);
        }

        [Fact]
        public async void Second_Loop()
        {
            var cron = Cron.Secondly().ParseCronExpression();
            var count = 3;
            var now = DateTimeOffset.Now;
            while (count-- > 0)
            {
                var next = cron.GetNextExecution();
                _helper.WriteLine($"{now} - {next}");
                await Task.Delay(1000);
            }
        }

        [Fact]
        public void Exception_Ok()
        {
            Assert.ThrowsAny<CronFormatException>(() => "61 * * * * *".ParseCronExpression().GetNextExecution());
        }
    }
}
