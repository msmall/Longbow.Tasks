﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Longbow.Tasks
{
    /// <summary>
    /// 调度执行实体类
    /// </summary>
    internal class SchedulerProcess
    {
        private readonly DefaultScheduler _sche;

        /// <summary>
        /// 默认构造函数
        /// </summary>
        /// <param name="sche">IScheduler 调度实例</param>
        public SchedulerProcess(DefaultScheduler sche) => _sche = sche;

        /// <summary>
        /// 获得/设置 任务调度状态
        /// </summary>
        public SchedulerStatus Status { get; set; }

        /// <summary>
        /// 获得/设置 任务持久化实例
        /// </summary>
        public IStorage Storage { get; set; }

        /// <summary>
        /// 获得/设置 任务调度实例
        /// </summary>
        public IScheduler Scheduler { get => _sche; }

        /// <summary>
        /// 获得/设置 调度任务
        /// </summary>
        public DefaultTaskMetaData TaskContext { get; private set; }

        /// <summary>
        /// 获得 所有触发器执行实例
        /// </summary>
        public List<TriggerProcess> Triggers { get; } = new List<TriggerProcess>();

        /// <summary>
        /// 日志委托
        /// </summary>
        public Action<string> LoggerAction { get; set; }

        /// <summary>
        /// 调度取消令牌
        /// </summary>
        private CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        /// 任务构造函数初始化取消令牌
        /// </summary>
        private CancellationTokenSource _initToken;

        /// <summary>
        /// 调度开始 每次调用
        /// </summary>
        /// <param name="trigger">ITrigger 实例</param>
        public void Start<T>(ITrigger trigger) where T : ITask, new()
        {
            _initToken = new CancellationTokenSource();

            // 泛型 T 的构造函数可能耗时很长 关注相关单元测试 TaskManagerTest -> LongDelayTrigger_Ok
            var sw = Stopwatch.StartNew();
            Task.Factory.StartNew(() =>
            {
                TaskContext = new DefaultTaskMetaData(new T());
                _sche.Task = TaskContext.Task;
                if (_cancellationTokenSource != null && _cancellationTokenSource.IsCancellationRequested)
                {
                    // Stop 调用
                    return;
                }
                _initToken.Cancel();
                LoggerAction($"{nameof(SchedulerProcess)} Start<{typeof(T).Name}> new({typeof(T).Name}) ThreadId({Thread.CurrentThread.ManagedThreadId})");
            }, TaskCreationOptions.LongRunning).ConfigureAwait(false);
            InternalStart(trigger);
            sw.Stop();
            LoggerAction($"{nameof(SchedulerProcess)} Start<{typeof(T).Name}> success Called Elapsed: {sw.Elapsed}");
        }

        /// <summary>
        /// 调度开始
        /// </summary>
        /// <param name="task">ITask 实例</param>
        /// <param name="trigger">ITrigger 实例</param>
        public void Start(ITask task, ITrigger trigger)
        {
            _initToken = new CancellationTokenSource();
            _initToken.Cancel();
            var sw = Stopwatch.StartNew();
            TaskContext = new DefaultTaskMetaData(task);
            _sche.Task = TaskContext.Task;
            InternalStart(trigger);
            sw.Stop();
            LoggerAction($"{nameof(SchedulerProcess)} Start(methodCall) success Called Elapsed: {sw.Elapsed}");
        }

        private void InternalStart(ITrigger trigger)
        {
            var triggerProcess = new TriggerProcess
            {
                Name = Scheduler.Name,
                LoggerAction = LoggerAction,
                Trigger = trigger,
                Storage = Storage,
                DoWork = async token =>
                {
                    _sche.Exception = null;
                    // 设置任务超时取消令牌
                    var taskCancelTokenSource = new CancellationTokenSource(trigger.Timeout);
                    try
                    {
                        // 保证 ITask 的 new() 方法被执行完毕
                        _initToken.Token.WaitHandle.WaitOne();

                        var taskToken = CancellationTokenSource.CreateLinkedTokenSource(token, taskCancelTokenSource.Token);
                        if (!taskToken.IsCancellationRequested)
                        {
                            await TaskContext.Execute(taskToken.Token).ConfigureAwait(false);
                            trigger.LastResult = TriggerResult.Success;
                        }
                    }
                    catch (TaskCanceledException) { }
                    catch (Exception ex)
                    {
                        _sche.Exception = ex;
                        ex.Log();
                    }

                    // 设置 Trigger 状态
                    if (token.IsCancellationRequested) trigger.LastResult = TriggerResult.Cancelled;
                    if (taskCancelTokenSource.IsCancellationRequested) trigger.LastResult = TriggerResult.Timeout;
                    if (_sche.Exception != null) trigger.LastResult = TriggerResult.Error;

                    // 取所有触发器中时间最近的
                    _sche.LastRunResult = $"{Scheduler.LastRuntime}: DefaultScheduler({Scheduler.Name}) Trigger({trigger.GetType().Name}) Finished({trigger.LastResult})";
                    LoggerAction($"{trigger.GetType().Name} LastRunResult: {Scheduler.LastRunResult} NextRuntime: {Scheduler.NextRuntime}");
                }
            };
            Triggers.Add(triggerProcess);

            // 注册触发器状态改变回调方法
            trigger.EnabeldChanged = enabled =>
            {
                LoggerAction($"{nameof(TriggerProcess)} Trigger({trigger.GetType().Name}) Enabled({enabled})");
                if (!enabled)
                {
                    triggerProcess.Stop();
                    return;
                }
                if (Status == SchedulerStatus.Running) triggerProcess.Start(_cancellationTokenSource.Token);
            };
            if (Status != SchedulerStatus.Disabled)
            {
                Status = SchedulerStatus.Running;
                _cancellationTokenSource = new CancellationTokenSource();
                triggerProcess.Start(_cancellationTokenSource.Token);
            }
        }

        /// <summary>
        /// 所有 调度开始
        /// </summary>
        public void Start()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            Triggers.ForEach(t => t.Start(_cancellationTokenSource.Token));
        }

        /// <summary>
        /// 调度停止
        /// </summary>
        public void Stop()
        {
            _cancellationTokenSource.Cancel();
            _initToken.Cancel();
            LoggerAction($"{nameof(TriggerProcess)} Stop() Called");
        }
    }
}
