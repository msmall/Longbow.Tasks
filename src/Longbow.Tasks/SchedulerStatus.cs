﻿namespace Longbow.Tasks
{
    /// <summary>
    /// 
    /// </summary>
    public enum SchedulerStatus
    {
        /// <summary>
        /// 
        /// </summary>
        Ready = 0,

        /// <summary>
        /// 
        /// </summary>
        Running = 1,

        /// <summary>
        /// 
        /// </summary>
        Disabled = 2
    }
}
