﻿# Longbow.Tasks

<a href="README.md">English</a> | <span>中文</span>

---

##### Gitee
[![Appveyor build](https://img.shields.io/endpoint.svg?logo=appveyor&label=build&color=blueviolet&url=https://ba.sdgxgz.com/api/Gitee/Builds?projName=longbow-tasks)](https://ci.appveyor.com/project/ArgoZhang/longbow-tasks)
[![Build Status](https://img.shields.io/appveyor/ci/ArgoZhang/longbow-tasks/master.svg?logo=appveyor&label=maser)](https://ci.appveyor.com/project/ArgoZhang/longbow-tasks)
[![Test](https://img.shields.io/appveyor/tests/ArgoZhang/longbow-tasks/master.svg?logo=appveyor&)](https://ci.appveyor.com/project/ArgoZhang/longbow-tasks/build/tests)
[![Issue Status](https://img.shields.io/endpoint.svg?logo=Groupon&logoColor=critical&label=issues&url=https://ba.sdgxgz.com/api/Gitee/Issues?userName=LongbowGroup%26repoName=Longbow.Tasks)](https://gitee.com/LongbowGroup/Longbow.Tasks/issues)
[![Pull Status](https://img.shields.io/endpoint.svg?logo=Groupon&logoColor=green&color=success&label=pulls&url=https://ba.sdgxgz.com/api/Gitee/Pulls?userName=LongbowGroup%26repoName=Longbow.Tasks)](https://gitee.com/LongbowGroup/Longbow.Tasks/pulls)

## 组件介绍
本组件同时支持 .net framework 4.5+ 以及 .net standard 2.0

### 组件介绍

### 用法

#### NETCore 容器注入
```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddTaskServices();
}
```

#### NETFramework 4.5+  
```csharp
// 程序入口调用
TaskServicesManager.Init();
```

#### ITaskServicesFactory
后台任务服务工厂接口，内部实现类为 TaskServicesFactory 也继承了 IHostedService 所以组件通过 `services.AddTaskServices();` 将任务服务注入到 NETCore 容器中  

#### IScheduler
后台任务调度接口，内部实现类为 DefaultScheduler 负责管理任务的调度  

#### ITrigger
后台任务触发器接口，内部内置三个实现类分别为 （默认触发器仅触发一次）DefaultTrigger （周期性定时触发器）RecurringTrigger （Cron表达式触发器）CronTrigger 可以通过实现 ITrigger 接口根据实际业务需要自行扩展触发器，组件默认提供 TriggerBuilder 负责创建任务触发器  


#### ITask
后台任务业务类接口，仅一个 `Task Execute(CancellationToken cancellationToken);` 方法，后台任务具体实现  

#### TaskServicesOptions
后台任务服务配置类  

#### TaskServicesManager: 
后台任务服务人机交互操作类，提供所有后台任务操作相关 API  

### 用法介绍  
```csharp
/// <summary>
/// 后台任务实现类
/// </summary>
public class FooTask : ITask
{
    /// <summary>
    /// 后台任务具体业务操作
    /// </summary>
    public async Task Execute(CancellationToken cancellationToken)
    {
        // 模拟任务执行耗时500毫秒
        try
        {
            await Task.Delay(500, cancellationToken);
        }
        catch (TaskCanceledException) { }
        if (cancellationToken.IsCancellationRequested)
        {
            return;
        }
        // do something ...
    }
}
```
用户通过调用 `TaskServicesManager.GetOrAdd<FooTask>()` 将 ITask 实现类 FooTask 添加到内部调度中，由于未给 ITrigger 参数，内部使用默认 DefaultTrigger 触发器，更多重载请查看源码（群文件或者码云仓库提供）  
TaskServicesManager 负责将任务与一个任务调度（IScheduler）与多个任务触发器（ITrigger）进行绑定，触发器负责任务的具体执行操作  

TaskServicesManager.GetOrAdd方法完全支持 lambda 表达式匿名函数  
```c#
TaskServicesManager.GetOrAdd("Default", async token =>
{
    try
    {
        await Task.Delay(500, token);
    }
    catch (TaskCanceledException) { }
});
```

### 例子

1. 默认任务 (立即执行，仅执行一次)

```c#
TaskServicesManager.GetOrAdd("简单任务1", token => Task.Delay(1000));
TaskServicesManager.GetOrAdd("简单任务2", token => Task.Delay(1000), TriggerBuilder.Default.Build());
```

2. 周期性任务 (1 分钟后间隔 5 秒执行2次任务)

```c#
var trigger = TriggerBuilder.Default.WithInterval(TimeSpan.FromSeconds(5)).WithRepeatCount(2).WithStartTime(DateTimeOffset.Now.AddMinutes(1)).Build(); 
TaskServicesManager.GetOrAdd("周期任务", token => Task.Delay(1000), trigger);
```

3. Cron表达式任务

#### Cron 格式说明

cron 表达式是用于定义固定时间、日期和间隔的掩码。掩码由秒（可选）、分钟、小时、日、月和星期字段组成。所有字段都允许指定多个值，如果所有字段都包含匹配的值，则任何给定的日期/时间都将满足指定的 cron 表达式。

                                           Allowed values    Allowed special characters   Comment

    ┌───────────── second (optional)       0-59              * , - /                      
    │ ┌───────────── minute                0-59              * , - /                      
    │ │ ┌───────────── hour                0-23              * , - /                      
    │ │ │ ┌───────────── day of month      1-31              * , - / L W ?                
    │ │ │ │ ┌───────────── month           1-12 or JAN-DEC   * , - /                      
    │ │ │ │ │ ┌───────────── day of week   0-6  or SUN-SAT   * , - / # L ?                Both 0 and 7 means SUN
    │ │ │ │ │ │
    * * * * * *

```c#
// 每秒钟执行一次任务
TaskServicesManager.GetOrAdd("表达式任务", token => Task.Delay(1000), TriggerBuilder.Build(Cron.Secondly()));

// 每分钟执行一次任务
TaskServicesManager.GetOrAdd("表达式任务", token => Task.Delay(1000), TriggerBuilder.Build(Cron.Minutely()));

// 间隔 5 秒循环执行任务
TaskServicesManager.GetOrAdd("表达式任务", token => Task.Delay(1000), TriggerBuilder.Build("*/5 * * * * *"));

// 每分钟的第 5 秒循环执行任务
TaskServicesManager.GetOrAdd("表达式任务", token => Task.Delay(1000), TriggerBuilder.Build("5 * * * * *"));
```

组件内置 Cron 表达式集成的是 [HangfirIO/Cronos](https://github.com/HangfireIO/Cronos) 更多关于 Cron 表达式的语法可以自行百度或者 Github 仓库查阅

### 任务持久化

任务持久化是指任务所在进程关闭后或者计算机重启后能够继续上一次执行的一直保持手段，可以将任务保存在文件中或者数据库中，目前系统内置物理文件持久化

#### 物理文件持久化

```csharp
/// <summary>
/// 物理文件持久化配置类
/// </summary>
public class FileStorageOptions
{
    /// <summary>
    /// 获得/设置 是否启用物理文件持久化 默认 true 启用
    /// </summary>
    public bool Enabled { get; set; } = true;

    /// <summary>
    /// 获得/设置 物理文件持久化文件目录名称 默认 TaskStorage
    /// </summary>
    public string Folder { get; set; } = "TaskStorage";

    /// <summary>
    /// 获得/设置 是否对持久化文件加密 默认为 true
    /// </summary>
    public bool Secure { get; set; } = true;

    /// <summary>
    /// 获得/设置 加密解密密钥
    /// </summary>
    public string Key { get; set; } = "LIBSFjql+0qPjAjBaQYQ9Ka2oWkzR1j6";

    /// <summary>
    /// 获得/设置 加密解密向量值
    /// </summary>
    public string IV { get; set; } = "rNWuCRQAWjI=";

    /// <summary>
    /// 获得/设置 任务被移除时是否删除持久化文件
    /// </summary>
    public bool DeleteFileByRemoveEvent { get; set; } = true;
}
```

使用时可以通过在配置文件进行自定义配置

```json
"FileStorageOptions": {
  "Enabled": true,
  "Folder": "TaskStorage",
  "Secure": true
}
```

#### 数据库持久化

*待开发（TBD）* 

## 总结
一行代码 **`TaskServicesManager.GetOrAdd<T>()`** 调度你的后台任务，彪悍的代码无需解释，更多问题请加群咨询群主作者

## 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
